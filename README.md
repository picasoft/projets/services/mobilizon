## Mobilizon

[[_TOC_]]

### Procédure de mise-à-jour

Pour changer la version de Mobilizon, il suffit de modifier le tag de l'image docker [`framasoft/mobilizon`](https://hub.docker.com/r/framasoft/mobilizon/tags) et suivre les instructions de mises-à-jour éventuelles sur la [page de release](https://framagit.org/framasoft/mobilizon/-/tags).

### Dépendances

Ce project est dépendant d'un autre projet dédié à la détection de SPAM.
C'est pourquoi il est indispensable d'avoir le sous-module prêt.

```bash
git submodule update --remote mobilizon-spam-detector
```


### Commandes en console utiles

Pour accéder à toutes les commandes possible, entrer dans l'image via
```
docker-compose exec mobilizon mobilizon_ctl nom_commande parametres...
```

#### Ajout d'un utilisateur avec les droits admin ou modérateur

Actuellement, il n'est pas possible de créer un compte administrateur ou modérateur depuis l'interface web, il est nécessaire de d'exécuter la commande suivante dans le conteneur `mobilizon-app` :
```
users.new email@host.pica --admin --password notS0Secre1Passwd
```
ou (exclusif)
```
users.new email@host.pica --moderator --password notS0Secre1Passwd
```

#### Modification d'un utilisateur

Pour modifier le rôle d'un utilisateur déjà existant, se reporter à la [commande `users.modify`](https://framagit.org/framasoft/mobilizon/-/blob/master/lib/mix/tasks/mobilizon/users/modify.ex).

#### Liste de tous les évènements locaux pour modération

```bash
docker compose exec mobilizon-db \
	psql -U mobilizon postgres_mobilizon -c \
		"select uuid from events where local = true;" | \
	sed 's#^ #https://mobilizon.picasoft.net/events/#' > all_events
```
