#!/bin/bash
# get big list from github and merge it with our own
#####################################################

# 0. get from github
####################
curl \
    https://raw.githubusercontent.com/disposable-email-domains/disposable-email-domains/master/disposable_email_blocklist.conf \
    --output \
    /github_email_blocklist.txt

# 1. merge it with our own list into the one used by spam detector
cat /github_email_blocklist.txt /app/emails_domains_spam.txt > /merged_email_blocklist.txt

# 2. export the variable for main.sh execution
export MOBILIZON_SPAM_FILE_EMAILS_DOMAINS=/merged_email_blocklist.txt
